package com.szagurskii.realmmigrationneeded;

import android.app.Activity;
import android.os.Bundle;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.szagurskii.realmmigrationneeded.sqlite.SQLiteAsyncTask;

public class MainActivity extends Activity {

    private TextView textViewUserCount;
    private TextView textViewRealmFileSize;
    private TextView textViewQueryExecTime;
    private BaseAsyncTask baseAsyncTask;

    private boolean animated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        animated = false;
        textViewUserCount = (TextView) findViewById(R.id.tv_user_count);
        textViewRealmFileSize = (TextView) findViewById(R.id.tv_realmfile_size);
        textViewQueryExecTime = (TextView) findViewById(R.id.tv_query_exec_time);
    }

    @Override
    protected void onResume() {
        super.onResume();

        baseAsyncTask = new SQLiteAsyncTask(new BaseAsyncTask.CounterUpdater() {
            @Override
            public void onUpdateUserCount(String value) {
                textViewUserCount.setText(value);
            }

            @Override
            public void onUpdateFileSize(final String value) {
                if (!animated) {
                    animated = true;

                    textViewRealmFileSize.post(new Runnable() {
                        @Override
                        public void run() {
                            textViewRealmFileSize.setText(value);
                        }
                    });
                    textViewRealmFileSize.post(new Runnable() {
                        @Override
                        public void run() {
                            Animation animation = AnimationUtils.loadAnimation(textViewRealmFileSize.getContext(), R.anim.animation_textview);
                            textViewRealmFileSize.setAnimation(animation);
                        }
                    });
                } else {
                    textViewRealmFileSize.setText(value);
                }
            }

            @Override
            public void onUpdateQueryExecTime(String value) {
                textViewQueryExecTime.setText(value);
            }
        }, this);
        baseAsyncTask.execute();

        textViewRealmFileSize.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                textViewRealmFileSize.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        animated = false;
        textViewRealmFileSize.clearAnimation();
        baseAsyncTask.cancel(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        textViewUserCount = null;
        textViewRealmFileSize = null;
        baseAsyncTask = null;
    }
}
