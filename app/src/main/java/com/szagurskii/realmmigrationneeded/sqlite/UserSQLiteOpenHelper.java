package com.szagurskii.realmmigrationneeded.sqlite;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * @author Savelii Zagurskii
 */
public class UserSQLiteOpenHelper extends SQLiteOpenHelper {

    private static final String TAG = UserSQLiteOpenHelper.class.getSimpleName();

    private static final String INTEGER_TYPE = " INTEGER";
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String DROP_TABLE_IF_EXISTS = "DROP TABLE IF EXISTS ";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + User.TABLE_NAME + " (" +
                    User.COLUMN_ID + INTEGER_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    User.COLUMN_NAME + TEXT_TYPE +
                    " )";

    private static final String SQL_EXECUTION_TABLE =
            "CREATE TABLE " + ExecutionTime.TABLE_NAME + " (" +
                    ExecutionTime.COLUMN_EXECUTION_TIME + INTEGER_TYPE +
                    " )";

    private static final String SQL_DELETE_ENTRIES = DROP_TABLE_IF_EXISTS + User.TABLE_NAME;
    private static final String SQL_DELETE_EXECUTION_TIME = DROP_TABLE_IF_EXISTS + ExecutionTime.TABLE_NAME;

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "Users.db";

    public UserSQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, new DatabaseErrorHandler() {
            @Override
            public void onCorruption(SQLiteDatabase dbObj) {
                Log.e(TAG, "Fatal corruption.");
            }
        });
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
        db.execSQL(SQL_EXECUTION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over.
        db.execSQL(SQL_DELETE_ENTRIES);
        db.execSQL(SQL_DELETE_EXECUTION_TIME);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
