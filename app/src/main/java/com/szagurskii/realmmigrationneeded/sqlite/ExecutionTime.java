package com.szagurskii.realmmigrationneeded.sqlite;

/**
 * @author Savelii Zagurskii
 */
public class ExecutionTime {

    public static final String TABLE_NAME = "EXECUTION_TIME_TABLE";

    public static final String COLUMN_EXECUTION_TIME = "executionTime";

    private String executionTime;

    public String getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(String executionTime) {
        this.executionTime = executionTime;
    }
}
