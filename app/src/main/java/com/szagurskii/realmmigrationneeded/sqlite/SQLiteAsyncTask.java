package com.szagurskii.realmmigrationneeded.sqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.szagurskii.realmmigrationneeded.BaseAsyncTask;

import java.io.File;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author Savelii Zagurskii
 */
public class SQLiteAsyncTask extends BaseAsyncTask {

    private static final String TAG = SQLiteAsyncTask.class.getSimpleName();

    private Context context;

    public SQLiteAsyncTask(CounterUpdater counterUpdater, Context context) {
        super(counterUpdater);
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... params) {
        final String sql = "INSERT INTO " + User.TABLE_NAME + " VALUES ";
        final String execTime = "INSERT INTO " + ExecutionTime.TABLE_NAME + " VALUES (\'%1$s\');";

        long start;
        long end;

        Random random = new Random();

        // Get the SQLite helper.
        UserSQLiteOpenHelper mDbHelper = new UserSQLiteOpenHelper(context);

        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        File file = new File(db.getPath());

        double fileMbLength;

        while (!isCancelled()) {
            fileMbLength = ((double) file.length()) / 1024d / 1024d;

            sendProgress(db, file, fileMbLength, average(db));

            Log.d(TAG, "Starting transaction.");

            start = System.nanoTime();
            db.beginTransaction();
            try {
                final int count = 450;
                StringBuilder builder = new StringBuilder(sql);
                for (int i = 0; i < count; i++, builder.append(",")) {
                    builder.append("(?,?)");
                }
                builder.deleteCharAt(builder.length() - 1);
                builder.append(";");

                SQLiteStatement statement;
                for (int j = 0; j < 100; j++) {
                    statement = db.compileStatement(builder.toString());
                    for (int i = 0; i < count; i = i + 2) {
                        statement.bindLong(i + 1, random.nextLong());
                        statement.bindString(i + 2, UUID.randomUUID().toString());
                    }
                    statement.execute();
                    statement.close();
                }
                db.setTransactionSuccessful();

                Log.d(TAG, "Transaction successful.");

            } catch (OutOfMemoryError | Exception error) {
                Log.e(TAG, "Fatal.", error);

                cancel(true);
            } finally {
                db.endTransaction();

                end = System.nanoTime();
                long elapsedMillis = TimeUnit.NANOSECONDS.toMillis(end - start);
                Log.i(TAG, String.format("Time elapsed: %1$s ms", elapsedMillis));

                saveAverage(execTime, db, elapsedMillis);
            }

            fileMbLength = ((double) file.length()) / 1024d / 1024d;

            sendProgress(db, file, fileMbLength, average(db));
        }

        db.close();
        mDbHelper.close();

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        context = null;
    }

    private void saveAverage(String execTime, SQLiteDatabase db, long elapsedMillis) {
        db.execSQL(String.format(execTime, elapsedMillis));
    }

    private void sendProgress(SQLiteDatabase db, File file, double fileMbLength, double average) {
        boolean gb = false;
        if (fileMbLength > 1024d) {
            fileMbLength = fileMbLength / 1024d;
            gb = true;
        }

        publishProgress(String.format("%1$s users", userCount(db)),
                        String.format("%1$.2f %4$s\n" +
                                              "Can read: %2$s\n" +
                                              "Can write: %3$s", fileMbLength, file.canRead(), file.canWrite(), gb ? "GB" : "MB"),
                        String.format("%1$.2f ms", average));
    }

    private long userCount(SQLiteDatabase database) {
        Cursor mCount = database.rawQuery(String.format("select count(*) from %1$s", User.TABLE_NAME), null);
        mCount.moveToFirst();
        long count = mCount.getLong(0);
        mCount.close();
        return count;
    }

    private double average(SQLiteDatabase database) {
        Cursor mCount = database.rawQuery(String.format("select avg(%2$s) from %1$s", ExecutionTime.TABLE_NAME, ExecutionTime.COLUMN_EXECUTION_TIME), null);
        mCount.moveToFirst();
        double count = mCount.getDouble(0);
        mCount.close();
        return count;
    }
}
