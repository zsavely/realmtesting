package com.szagurskii.realmmigrationneeded.sqlite;

/**
 * @author Savelii Zagurskii
 */
public class User {

    public static final String TABLE_NAME = "USER_TABLE";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";

    private long id;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
