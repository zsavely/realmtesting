package com.szagurskii.realmmigrationneeded.realm;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

/**
 * @author Savelii Zagurskii
 */
public class RealmGreatMigration implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        // DynamicRealm exposes an editable schema.
        RealmSchema schema = realm.getSchema();

        // Migrate to version 1: Add a new class.
        if (newVersion != oldVersion) {
            schema.get(RealmUser.class.getSimpleName())
                    .addField(RealmUser.COLUMN_id, long.class);
            oldVersion++;
        }
    }
}
