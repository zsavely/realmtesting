package com.szagurskii.realmmigrationneeded.realm;

import io.realm.annotations.RealmModule;

/**
 * @author Savelii Zagurskii
 */
@RealmModule(classes = {RealmUser.class})
public class RealmUserModule {
}
