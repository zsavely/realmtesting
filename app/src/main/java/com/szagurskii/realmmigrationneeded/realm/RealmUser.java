package com.szagurskii.realmmigrationneeded.realm;

import io.realm.RealmObject;

/**
 * @author Savelii Zagurskii
 */
public class RealmUser extends RealmObject {

    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_id = "id";

    private long id;
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
