package com.szagurskii.realmmigrationneeded.realm;

import android.util.Log;

import com.szagurskii.realmmigrationneeded.BaseAsyncTask;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import io.realm.Realm;
import io.realm.exceptions.RealmError;

/**
 * @author Savelii Zagurskii
 */
public class RealmAsyncTask extends BaseAsyncTask {

    private static final String TAG = BaseAsyncTask.class.getSimpleName();

    public RealmAsyncTask(CounterUpdater counterUpdater) {
        super(counterUpdater);
    }

    @Override
    protected Void doInBackground(Void... params) {
        Realm realm = Realm.getDefaultInstance();
        Random random = new Random();
        List<RealmUser> realmUsers = new ArrayList<>();
        File file = new File(realm.getConfiguration().getPath());
        double fileMbLength;

        while (!isCancelled()) {
            fileMbLength = ((double) file.length()) / 1024d / 1024d;

            publishProgress(String.format("%1$s users", realm.where(RealmUser.class).count()),
                    String.format("%1$s MB\n" +
                            "Can read: %2$s\n" +
                            "Can write: %3$s", fileMbLength, file.canRead(), file.canWrite()));

            for (long i = 0; i < 25000; i++) {
                RealmUser user = new RealmUser();

                user.setName(UUID.randomUUID().toString());
                user.setId(random.nextLong());

                realmUsers.add(user);
            }

            Log.d(TAG, "Starting transaction..");

            try {
                realm.beginTransaction();
                realm.copyToRealm(realmUsers);
                realm.commitTransaction();

                Log.d(TAG, "Transaction committed.");

                clear(realmUsers);
            } catch (RealmError | OutOfMemoryError error) {
                Log.e(TAG, "Fatal.", error);
                realm.cancelTransaction();

                clear(realmUsers);

                return null;
            }

            fileMbLength = ((double) file.length()) / 1024d / 1024d;

            publishProgress(String.format("%1$s users", realm.where(RealmUser.class).count()),
                    String.format("%1$s MB\n" +
                            "Can read: %2$s\n" +
                            "Can write: %3$s", fileMbLength, file.canRead(), file.canWrite()));
        }

        realm.close();

        return null;
    }

    private void clear(List<RealmUser> realmUsers) {
        Log.d(TAG, "Clearing list..");
        realmUsers.clear();
        Log.d(TAG, "List cleared.");

        Log.d(TAG, "Running GC..");
        System.gc();
        Log.d(TAG, "GC ran.");
    }

}
