package com.szagurskii.realmmigrationneeded;

import android.app.Application;
import android.os.Environment;

import com.szagurskii.realmmigrationneeded.realm.RealmGreatMigration;
import com.szagurskii.realmmigrationneeded.realm.RealmUserModule;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * @author Savelii Zagurskii
 */
public class RealmApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        File fileExternalRealm = new File(String.format("%1$s/%2$s/", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath(), BuildConfig.APPLICATION_ID));
        boolean dirMade = fileExternalRealm.mkdirs();

        RealmConfiguration externalConfig = new RealmConfiguration.Builder(fileExternalRealm)
                .name("chat.realm")
                .deleteRealmIfMigrationNeeded()
                .schemaVersion(35)
                .migration(new RealmGreatMigration())
                .setModules(new RealmUserModule())
                .build();
        //Realm.compactRealm(externalConfig);
        Realm.setDefaultConfiguration(externalConfig);
    }

    private void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }
}
