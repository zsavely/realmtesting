package com.szagurskii.realmmigrationneeded;

import android.os.AsyncTask;

/**
 * @author Savelii Zagurskii
 */
public abstract class BaseAsyncTask extends AsyncTask<Void, String, Void> {

    private CounterUpdater counterUpdater;

    public BaseAsyncTask(CounterUpdater counterUpdater) {
        super();

        this.counterUpdater = counterUpdater;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        counterUpdater.onUpdateUserCount(values[0]);
        counterUpdater.onUpdateFileSize(values[1]);
        counterUpdater.onUpdateQueryExecTime(values[2]);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        counterUpdater = null;
    }

    public interface CounterUpdater {
        void onUpdateUserCount(String value);

        void onUpdateFileSize(String value);

        void onUpdateQueryExecTime(String value);
    }
}
